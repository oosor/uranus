FROM ubuntu:20.04

MAINTAINER Andrew Kris <3oosor@gmail.com>

ENV DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Kiev
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

ARG AsteriskVersion
ARG PathAsterisk=/opt/asterisk-${AsteriskVersion}
ARG UserAsterisk=asterisk

# Running update and install makes the builder not to use cache which resolves some updates
RUN apt-get update && apt-get install -y linux-headers-$(uname -r) wget net-tools build-essential git bc libssl-dev autoconf subversion pkg-config libtool

# First, download the latest version of DAHDI to the /opt directory:
RUN cd /opt \
    && git clone -b next git://git.asterisk.org/dahdi/linux dahdi-linux \
    && cd dahdi-linux \
    && make \
    && make install

# Next, download the DAHDI tools with the following command:
RUN cd /opt \
    && git clone -b next git://git.asterisk.org/dahdi/tools dahdi-tools \
    && cd dahdi-tools \
    && autoreconf -i \
    && ./configure \
    && make install \
    && make install-config \
    && dahdi_genconf modules

# Next, download the LibPRI to communicate Asterisk with ISDN connections.
RUN cd /opt \
    && git clone https://gerrit.asterisk.org/libpri libpri \
    && cd libpri \
    && make \
    && make install

# Install Asterisk
RUN cd /opt \
    && git clone -b ${AsteriskVersion} https://gerrit.asterisk.org/asterisk asterisk-${AsteriskVersion} \
    && cd asterisk-${AsteriskVersion} \
    && contrib/scripts/get_mp3_source.sh \
    && contrib/scripts/install_prereq install

RUN cd ${PathAsterisk} \
    && ./configure --with-resample --with-pjproject-bundled --with-jansson-bundled

#see https://github.com/tiredofit/docker-freepbx/blob/15/Dockerfile

RUN cd ${PathAsterisk} \
    && make menuselect/menuselect menuselect-tree menuselect.makeopts \
    && menuselect/menuselect --disable BUILD_NATIVE menuselect.makeopts \
    && menuselect/menuselect --enable BETTER_BACKTRACES menuselect.makeopts \
    && menuselect/menuselect --enable chan_ooh323 menuselect.makeopts

# codecs
# menuselect/menuselect --enable codec_opus menuselect.makeopts
# menuselect/menuselect --enable codec_silk menuselect.makeopts

# # download more sounds
# for i in CORE-SOUNDS-EN MOH-OPSOUND EXTRA-SOUNDS-EN; do
#   for j in ULAW ALAW G722 GSM SLN16; do
#     menuselect/menuselect --enable $i-$j menuselect.makeopts
#   done
# done

RUN cd ${PathAsterisk} \
    && make -j2 \
    && make install

RUN cd ${PathAsterisk} \
    && make samples \
    && make basic-pbx \
    && make config \
    && ldconfig

RUN adduser --system --group --home /var/lib/asterisk --no-create-home --gecos "Asterisk PBX" ${UserAsterisk}

RUN sed -i "s|#AST_USER=\"asterisk\"|AST_USER=\"asterisk\" |g" /etc/default/asterisk \
    && sed -i "s|#AST_GROUP=\"asterisk\"|AST_GROUP=\"asterisk\" |g" /etc/default/asterisk

RUN usermod -a -G dialout,audio ${UserAsterisk}

RUN chown -R ${UserAsterisk}: /var/lib/asterisk /var/log/asterisk /var/run/asterisk /var/spool/asterisk /usr/lib/asterisk /etc/asterisk \
    && chmod -R 750 /var/lib/asterisk /var/log/asterisk /var/run/asterisk /var/spool/asterisk /usr/lib/asterisk /etc/asterisk

#ENTRYPOINT ["/usr/sbin/asterisk", "-T", "-W", "-U", "${UserAsterisk}", "-p", "-vvvdddf"]
CMD /usr/sbin/asterisk -T -W -U asterisk -p -vvvdddf
